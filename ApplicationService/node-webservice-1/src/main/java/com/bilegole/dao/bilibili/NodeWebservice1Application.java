package com.bilegole.dao.bilibili;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NodeWebservice1Application {

    public static void main(String[] args) {
        SpringApplication.run(NodeWebservice1Application.class, args);
    }

}
