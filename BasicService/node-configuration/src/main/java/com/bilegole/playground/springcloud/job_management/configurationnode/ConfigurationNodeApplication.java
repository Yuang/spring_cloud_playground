package com.bilegole.playground.springcloud.job_management.configurationnode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConfigurationNodeApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConfigurationNodeApplication.class, args);
    }

}
