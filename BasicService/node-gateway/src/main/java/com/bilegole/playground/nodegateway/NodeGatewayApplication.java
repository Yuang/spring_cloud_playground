package com.bilegole.playground.nodegateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NodeGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(NodeGatewayApplication.class, args);
    }

}
