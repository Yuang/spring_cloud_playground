package com.bilegole.playground.springcloud.job_management.noderegistry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class NodeRegistryApplication {

    public static void main(String[] args) {
        SpringApplication.run(NodeRegistryApplication.class, args);
    }

}
