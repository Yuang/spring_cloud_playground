package com.bilegole.dao.bilibili;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
public class BilibiliJpaDao {

    public static void main(String[] args) {
        SpringApplication.run(BilibiliJpaDao.class, args);
    }

}
