package com.bilegole.dao.bilibili.dao;

import com.bilegole.dao.bilibili.entity.User.BSex;
import com.bilegole.dao.bilibili.entity.User.BSexKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

@Component
public interface BSexReposity extends JpaRepository<BSexKey, BSex> {
}
