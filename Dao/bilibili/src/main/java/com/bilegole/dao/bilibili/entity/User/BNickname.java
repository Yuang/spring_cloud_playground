package com.bilegole.dao.bilibili.entity.User;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Getter
@Setter
//@ToString
//@RequiredArgsConstructor
@Entity
@Table(name = "b_nickname")
public class BNickname {
    @EmbeddedId
    private BNicknameKey key;

    @Column(name = "end_time", columnDefinition = "datetime")
    private Date end_time;

    @Column(name = "nickname", columnDefinition = "char(100)")
    private String nickname;
}
