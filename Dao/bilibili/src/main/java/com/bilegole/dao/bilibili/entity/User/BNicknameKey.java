package com.bilegole.dao.bilibili.entity.User;

import lombok.Data;
import org.hibernate.Hibernate;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Data
@Embeddable
public class BNicknameKey implements Serializable {
    private static final long serialVersionUID = -7523633817377892759L;

    @Embedded
    @Column(name = "id", columnDefinition = "char(100)")
    private String id;

    @Embedded
    @Column(name = "start_time", columnDefinition = "datetime")
    private Date start_time;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        BNicknameKey that = (BNicknameKey) o;
        return id != null && Objects.equals(id, that.id)
                && start_time != null && Objects.equals(start_time, that.start_time);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, start_time);
    }
}
