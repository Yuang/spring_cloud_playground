package com.bilegole.dao.bilibili.entity.User;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;
import java.util.Objects;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
@Table(name = "b_sex")
public class BSex {
    @EmbeddedId
    BSexKey key;

    @Column
    Date end_time;

    @Column
    String content;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        BSex bSex = (BSex) o;
        return key != null && Objects.equals(key, bSex.key);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key);
    }
}
