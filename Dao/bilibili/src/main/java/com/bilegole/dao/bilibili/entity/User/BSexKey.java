package com.bilegole.dao.bilibili.entity.User;

import lombok.Data;
import org.hibernate.Hibernate;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Data
@Embeddable
public class BSexKey implements Serializable {
    @Embedded
    @Column(name = "id")
    String id;

    @Embedded
    @Column(name = "id")
    Date start_time;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        BSexKey bSexKey = (BSexKey) o;
        return id != null && Objects.equals(id, bSexKey.id)
                && start_time != null && Objects.equals(start_time, bSexKey.start_time);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, start_time);
    }
}
