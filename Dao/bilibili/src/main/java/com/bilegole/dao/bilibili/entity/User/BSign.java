package com.bilegole.dao.bilibili.entity.User;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;
import java.util.Objects;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
@Table(name = "b_sign")
public class BSign {
    @EmbeddedId
    BSignKey key;

    @Column
    Date end_time;

    @Column
    String content;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        BSign bSign = (BSign) o;
        return key != null && Objects.equals(key, bSign.key);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key);
    }
}
