package com.bilegole.dao.bilibili.entity.User;

import lombok.Data;

import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import java.io.Serializable;
import java.util.Date;

@Data
@Embeddable
public class BSignKey implements Serializable {
    @Embedded
    String id;

    @Embedded
    Date start_time;
}
