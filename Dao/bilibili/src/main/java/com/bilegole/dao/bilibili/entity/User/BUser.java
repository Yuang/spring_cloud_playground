package com.bilegole.dao.bilibili.entity.User;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
@Table(name = "b_user")
public class BUser {
    @Id
    @Column(name = "id", columnDefinition = "char(100)")
    String id;

    @Column(name = "name", columnDefinition = "char(100)")
    String name;
//
//    @OneToMany
//    List<BSign> BSigns;
//
//    @OneToMany
//    List<BSex> BSexs;
//
//    @OneToMany
//    List<BNickname> BNicknames;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        BUser bUser = (BUser) o;
        return id != null && Objects.equals(id, bUser.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
