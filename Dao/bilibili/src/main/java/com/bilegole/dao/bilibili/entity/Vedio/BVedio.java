package com.bilegole.dao.bilibili.entity.Vedio;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
@Table(name = "vedio")
public class BVedio {
    @Id
    @Column(name = "id", nullable = false)
    private String id;

//    @OneToMany
//    @ToString.Exclude
//    private List<VDetail> VDetails;
//
//    @OneToMany
//    @ToString.Exclude
//    private List<VName> VNames;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        BVedio bVedio = (BVedio) o;
        return id != null && Objects.equals(id, bVedio.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
