package com.bilegole.dao.bilibili.entity.Vedio;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "vedio_detail")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class VDetail {
    //    @Id
//    @Column(name = "id", nullable = false)
//    private String id;
//
//    @Column
//    private String ownerId;
    @EmbeddedId
    VDetailKey key;

    @Column
    String detail;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VDetail vDetail = (VDetail) o;
        return Objects.equals(key, vDetail.key) && Objects.equals(detail, vDetail.detail);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key, detail);
    }
}
