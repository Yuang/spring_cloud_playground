package com.bilegole.dao.bilibili.entity.Vedio;

import lombok.Data;

import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import java.io.Serializable;
import java.util.Date;

@Embeddable
@Data
public class VDetailKey implements Serializable {
    @Embedded
    String id;

    @Embedded
    Date start_time;
}
