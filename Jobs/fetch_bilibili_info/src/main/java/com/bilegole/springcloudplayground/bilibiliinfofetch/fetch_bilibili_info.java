package com.bilegole.springcloudplayground.bilibiliinfofetch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class fetch_bilibili_info {

    public static void main(String[] args) {
        SpringApplication.run(fetch_bilibili_info.class, args);
    }

}
