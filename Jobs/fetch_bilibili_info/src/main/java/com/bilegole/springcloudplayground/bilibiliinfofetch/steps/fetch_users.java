package com.bilegole.springcloudplayground.bilibiliinfofetch.steps;

import org.springframework.batch.core.JobInterruptedException;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.StepExecution;

public class fetch_users implements Step {
    @Override
    public String getName() {
        return "fetch_users";
    }

    @Override
    public boolean isAllowStartIfComplete() {
        return true;
    }

    @Override
    public int getStartLimit() {
        return 0;
    }

    @Override
    public void execute(StepExecution stepExecution) throws JobInterruptedException {

    }
}
