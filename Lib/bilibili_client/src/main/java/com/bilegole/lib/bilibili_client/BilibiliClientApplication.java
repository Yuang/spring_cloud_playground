package com.bilegole.lib.bilibili_client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BilibiliClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(BilibiliClientApplication.class, args);
    }

}
