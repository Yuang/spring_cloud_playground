package com.bilegole.lib.bilibili_client;

import com.bilegole.lib.bilibili_client.enety.User;
import com.bilegole.lib.bilibili_client.enety.dto.UserBiliDto;
import com.bilegole.lib.bilibili_client.enety.dto.UserBiliFollowsDto;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

@Component
public class Client {
    protected final String main_page_uri = "https://api.bilibili.com/x/space/acc/info?mid=%s&jsonp=jsonp/";
    protected final String follow_info_uri = "https://api.bilibili.com/x/relation/followings?" +
            "vmid=%s&pn=%s&ps=%s&order_type=%s&jsonp=jsonp";
    protected final String referer = "https://space.bilibili.com/%s";
    WebClient client;
    User user;
    String mid;

    Client() {
        client = WebClient.create();
        user = new User();
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    String getMainPageUri(String mid) {
        return String.format(main_page_uri, mid);
    }

    String getFollowInfoUri(String mid, Integer pn, String order) {
        return String.format(follow_info_uri, mid, pn, 50, order);
    }

    String getReferer(String _mid) {
        return String.format(referer, _mid);
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public void fetch_main_info() {
        UserBiliDto userdto = client.get().uri(getMainPageUri(mid))
                .header("referer", getReferer(mid)).retrieve().bodyToMono(UserBiliDto.class).block();
        assert userdto != null;
        user.loadFromDto(userdto);
    }

    public void fetch_follows() {
        for (int i = 1; i < 6; i++) {
            UserBiliFollowsDto followsDto = client.get().uri(getFollowInfoUri(mid, i, "attention"))
                    .header("referer", getReferer(mid)).retrieve().bodyToMono(UserBiliFollowsDto.class).block();
            user.loadFromFollows(followsDto);
            followsDto = client.get().uri(getFollowInfoUri(mid, i, ""))
                    .header("referer", getReferer(mid)).retrieve().bodyToMono(UserBiliFollowsDto.class).block();
            user.loadFromFollows(followsDto);
        }
    }

    public void fetch(String mid) {
        setMid(mid);
        fetch_main_info();
        fetch_follows();
    }


}
