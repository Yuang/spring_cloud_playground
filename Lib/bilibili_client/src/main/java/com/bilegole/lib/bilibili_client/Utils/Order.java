package com.bilegole.lib.bilibili_client.Utils;

public enum Order {
    DESC("desc");
    private final String content;

    Order(String content) {
        this.content = content;
    }

    String getContent() {
        return content;
    }
}
