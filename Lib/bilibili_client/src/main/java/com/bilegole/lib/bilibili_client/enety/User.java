package com.bilegole.lib.bilibili_client.enety;

import com.bilegole.lib.bilibili_client.enety.dto.UserBiliDto;
import com.bilegole.lib.bilibili_client.enety.dto.UserBiliFollowsDto;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Data
@Slf4j
public class User {
    String name;
    String id;
    String sex;
    String sign;
    Set<String> follows;

    public User() {
        follows = new HashSet<>();
    }

    public void loadFromDto(UserBiliDto dto) {
        if (dto == null) {
            log.warn("dto is null");
            return;
        }
        name = dto.getData().getName();
        id = dto.getData().getMid();
        sex = dto.getData().getSex();
        sign = dto.getData().getSign();
    }

    public void loadFromFollows(UserBiliFollowsDto dto) {
        if (dto == null) {
            log.warn("dto is zero");
            System.out.println(Arrays.toString(Thread.currentThread().getStackTrace()));
            return;
        }
        for (int i = 0; i < dto.getData().getList().size(); i++) {
            follows.add(dto.getData().getList().get(i).getMid());
        }
        log.warn("dto size:" + dto.getData().getList().size());
        log.warn("user follow size:" + this.follows.size());
    }
}
