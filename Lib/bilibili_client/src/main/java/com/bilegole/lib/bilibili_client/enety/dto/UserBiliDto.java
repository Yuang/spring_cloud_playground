package com.bilegole.lib.bilibili_client.enety.dto;

import lombok.Data;

@Data
public class UserBiliDto {
    String code;
    UserBiliDtoData data;
    String message;
    Integer ttl;
}
