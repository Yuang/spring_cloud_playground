package com.bilegole.lib.bilibili_client.enety.dto;

import lombok.Data;

@Data
public class UserBiliDtoData {
    String birthday;
    Integer coins;
    String face;
    Integer level;
    String name;
    String Sign;
    String mid;
    String sex;
}
