package com.bilegole.lib.bilibili_client.enety.dto;

import lombok.Data;

@Data
public class UserBiliFollowsDto {
    Integer code;
    UserBiliFollowsDtoData data;
    String message;
    Integer ttl;
}
