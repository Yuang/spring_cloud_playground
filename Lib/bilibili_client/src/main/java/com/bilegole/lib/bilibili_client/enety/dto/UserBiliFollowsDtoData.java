package com.bilegole.lib.bilibili_client.enety.dto;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Data
@Slf4j
public class UserBiliFollowsDtoData {
    List<UserBiliFollowsDtoFan> list;
    Integer re_version;
    Integer total;
}
