package com.bilegole.lib.bilibili_client.enety.dto;

import lombok.Data;

@Data
public class UserBiliFollowsDtoFan {
    String mid;
    String uname;
    String sign;
}
