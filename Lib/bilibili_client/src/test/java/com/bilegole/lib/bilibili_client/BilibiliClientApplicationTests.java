package com.bilegole.lib.bilibili_client;

import com.bilegole.lib.bilibili_client.enety.User;
import com.bilegole.lib.bilibili_client.enety.dto.UserBiliDto;
import com.bilegole.lib.bilibili_client.enety.dto.UserBiliFollowsDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.Objects;

@SpringBootTest
class BilibiliClientApplicationTests {
    @Autowired
    Client client;

    //"https://api.bilibili.com/x/relation/followings?vmid=89374363&pn=1&ps=20&order=desc&order_type=attention&jsonp=jsonp&callback=__jp12"
    @Test
    void contextLoads() {
    }

    @Test
    void playground() {
        String body = client.client.get().uri("https://space.bilibili.com/1083063491/")
                .retrieve().bodyToMono(String.class).block();
        System.out.println(body + "\n----------------------------------\n");
        String body_2 = client.client.get().uri(uriBuilder -> uriBuilder
                        .scheme("https")
                        .host("api.bilibili.com")
                        .path("/x/relation/followings")
                        .queryParam("vmid", "89374363")
                        .queryParam("pn", "1")
                        .queryParam("ps", "20")
                        .queryParam("order", "desc")
                        .queryParam("order_type", "attention")
                        .queryParam("jsonp", "jsonp")
                        .queryParam("callback", "__jp12")
                        .build())
                .header("referer", "https://space.bilibili.com/1855205/fans/follow")
                .retrieve().bodyToMono(String.class).block();
        System.out.println(body_2);
    }

    @Test
    void fetch_main_info() {
//        client.fetch("1083063491");
        client.setMid("1083063491");
        client.fetch_main_info();
        User user = client.getUser();
        System.out.println(user);
    }

    @Test
    void fetch_main_info_without_class() {
        String mid = "1083063491";
        WebClient client = WebClient.create();
        WebClient.ResponseSpec spec = client.get().uri(String.format("https://api.bilibili.com/x/space/acc/info?mid=%s&jsonp=jsonp", mid))
                .header("referer", String.format("https://space.bilibili.com/%s/", mid))
                .retrieve();
        UserBiliDto dto = Objects.requireNonNull(spec.toEntity(UserBiliDto.class).block()).getBody();
        System.out.println(dto);
    }

    @Test
    void fetch_main_info_with_class() {
        String mid = "1083063491";
        client.setMid(mid);
        client.fetch_main_info();
        User user = client.getUser();
        System.out.println(user);
    }

    @Test
    void fetch_follows_without_class() {
        String mid = "1083063491";
        WebClient client = WebClient.create();
        WebClient.ResponseSpec spec = client.get()
                .uri(uriBuilder -> uriBuilder
                        .scheme("https")
                        .host("api.bilibili.com")
                        .path("/x/relation/followings")
                        .queryParam("vmid", mid)
                        .queryParam("pn", "1")
                        .queryParam("ps", "50")
                        .queryParam("order", "desc")
                        .queryParam("order_type", "attention")
                        .queryParam("jsonp", "jsonp")
//                        .queryParam("callback", "__jp4")
                        .build())
                .header("referer", String.format("https://space.bilibili.com/%s/fans/follow", mid))
                .retrieve();
        UserBiliFollowsDto dto = Objects.requireNonNull(spec.toEntity(UserBiliFollowsDto.class).block()).getBody();
        User user = new User();
        user.loadFromFollows(dto);
        System.out.println(user.getFollows());
    }

    @Test
    public void getUser() {
        String mid = "43634508";
        client.fetch(mid);
        User user = client.getUser();
        System.out.println(user);
        System.out.println(user.getFollows().size());
    }
}
